import { Loadout } from 'src/models/event';

export interface Battle {
    id: number;
    time: number;
    winners: BattlePlayer[];
    losers: BattlePlayer[];
}

export interface BattlePlayer {
    name: string;
    loadout: Loadout;
}
