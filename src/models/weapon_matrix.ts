export type WeaponMatrix = { [key: string]: { [key: string]: MatchupStats } };

export interface MatchupStats {
    wins: number,
    losses: number,
}
