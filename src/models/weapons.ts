import { useEffect, useState } from 'react';
import { BuildStats } from './build_stats';
import { WeaponMatrix } from './weapon_matrix';

export const weapons: { [id: string]: string } = {
    "": "Any",
    "MAIN_ARCANESTAFF": "Arcane Staff",
    "2H_KNUCKLES_SET2": "Battle Bracers",
    "MAIN_AXE": "Battleaxe",
    "2H_DUALAXE_KEEPER": "Bear Paws",
    "MAIN_ROCKMACE_KEEPER": "Bedrock Mace",
    // "2H_IRONGAUNTLETS_HELL": "Black Hands",
    "2H_COMBATSTAFF_MORGANA": "Black Monk Stave",
    "2H_INFERNOSTAFF_MORGANA": "Blazing Staff",
    "2H_NATURESTAFF_HELL": "Blight Staff",
    "MAIN_RAPIER_MORGANA": "Bloodletter",
    "2H_SHAPESHIFTER_MORGANA": "Bloodmoon Staff",
    "2H_DUALCROSSBOW_HELL": "Boltcasters",
    "2H_BOW_KEEPER": "Bow of Badon",
    "2H_BOW": "Bow",
    "2H_KNUCKLES_SET1": "Brawler Gloves",
    "2H_DAGGER_KATAR_AVALON": "Bridled Fury",
    "2H_FIRESTAFF_HELL": "Brimstone Staff",
    "MAIN_SWORD": "Broadsword",
    "2H_MACE_MORGANA": "Camlann Mace",
    "2H_HALBERD_MORGANA": "Carrioncaller",
    "2H_CLEAVER_HELL": "Carving Sword",
    "MAIN_FROSTSTAFF_AVALON": "Chillhowl",
    "MAIN_SCIMITAR_MORGANA": "Clarent Blade",
    "2H_CLAWPAIR": "Claws",
    "2H_CLAYMORE": "Claymore",
    "2H_CROSSBOW": "Crossbow",
    "2H_SKULLORB_HELL": "Cursed Skull",
    "MAIN_CURSEDSTAFF": "Cursed Staff",
    "2H_DAGGERPAIR": "Dagger Pair",
    "MAIN_DAGGER": "Dagger",
    "2H_CURSEDSTAFF_MORGANA": "Damnation Staff",
    "2H_FIRE_RINGPAIR_AVALON": "Dawnsong",
    "MAIN_SPEAR_LANCE_AVALON": "Daybreaker",
    "2H_DUALSICKLE_UNDEAD": "Deathgivers",
    "MAIN_DAGGER_HELL": "Demonfang",
    "2H_DEMONICSTAFF": "Demonic Staff",
    "2H_DIVINESTAFF": "Divine Staff",
    "2H_DOUBLEBLADEDSTAFF": "Double Bladed Staff",
    "MAIN_NATURESTAFF_KEEPER": "Druidic Staff",
    "2H_DUALSWORD": "Dual Swords",
    "2H_SHAPESHIFTER_KEEPER": "Earthrune Staff",
    "2H_CROSSBOW_CANNON_AVALON": "Energy Shaper",
    "2H_ENIGMATICSTAFF": "Enigmatic Staff",
    "2H_ARCANE_RINGPAIR_AVALON": "Evensong",
    "2H_HOLYSTAFF_HELL": "Fallen Staff",
    "MAIN_FIRESTAFF": "Fire Staff",
    "2H_KNUCKLES_AVALON": "Fists of Avalon",
    "2H_DUALHAMMER_HELL": "Forge Hammers",
    "MAIN_FROSTSTAFF": "Frost Staff",
    "2H_DUALSCIMITAR_UNDEAD": "Galatine Pair",
    "2H_GLACIALSTAFF": "Glacial Staff",
    "2H_GLAIVE": "Glaive",
    "2H_QUARTERSTAFF_AVALON": "Grailseeker",
    "2H_ARCANESTAFF": "Great Arcane Staff",
    "2H_CURSEDSTAFF": "Great Cursed Staff",
    "2H_FIRESTAFF": "Great Fire Staff",
    "2H_FROSTSTAFF": "Great Frost Staff",
    "2H_HAMMER": "Great Hammer",
    "2H_HOLYSTAFF": "Great Holy Staff",
    "2H_NATURESTAFF": "Great Nature Staff",
    "2H_AXE": "Greataxe",
    "2H_RAM_KEEPER": "Grovekeeper",
    "2H_HALBERD": "Halberd",
    "MAIN_HOLYSTAFF_AVALON": "Hallowfall",
    "MAIN_HAMMER": "Hammer",
    "2H_HAMMER_AVALON": "Hand of Justice",
    "2H_CROSSBOWLARGE": "Heavy Crossbow",
    "2H_MACE": "Heavy Mace",
    "2H_KNUCKLES_HELL": "Hellfire Hands",
    "2H_SHAPESHIFTER_HELL": "Hellspawn Staff",
    "MAIN_SPEAR_KEEPER": "Heron Spear",
    "MAIN_FROSTSTAFF_KEEPER": "Hoarfrost Staff",
    "MAIN_HOLYSTAFF": "Holy Staff",
    "2H_ICEGAUNTLETS_HELL": "Icicle Staff",
    "MAIN_MACE_HELL": "Incubus Mace",
    "2H_SCYTHE_HELL": "Infernal Scythe",
    "2H_INFERNOSTAFF": "Infernal Staff",
    "2H_IRONCLADEDSTAFF": "Iron-clad Staff",
    "MAIN_NATURESTAFF_AVALON": "Ironroot Staff",
    "2H_CLAYMORE_AVALON": "Kingmaker",
    "MAIN_CURSEDSTAFF_UNDEAD": "Lifecurse Staff",
    "MAIN_HOLYSTAFF_MORGANA": "Lifetouch Staff",
    "MAIN_1HCROSSBOW": "Light Crossbow",
    "2H_SHAPESHIFTER_AVALON": "Lightcaller",
    "2H_LONGBOW": "Longbow",
    "MAIN_MACE": "Mace",
    "2H_ENIGMATICORB_MORGANA": "Malevolent Locus",
    "2H_BOW_AVALON": "Mistpiercer",
    "2H_FLAIL": "Morning Star",
    "MAIN_NATURESTAFF": "Nature Staff",
    "2H_DUALMACE_AVALON": "Oathkeepers",
    "2H_ARCANESTAFF_HELL": "Occult Staff",
    "2H_ICECRYSTAL_UNDEAD": "Permafrost Prism",
    "2H_SPEAR": "Pike",
    "2H_POLEHAMMER": "Polehammer",
    "2H_SHAPESHIFTER_SET3": "Primal Staff",
    "2H_SHAPESHIFTER_SET1": "Prowling Staff",
    "2H_QUARTERSTAFF": "Quarterstaff",
    "2H_NATURESTAFF_KEEPER": "Rampant Staff",
    "2H_KNUCKLES_MORGANA": "Ravenstrike Cestus",
    "2H_AXE_AVALON": "Realmbreaker",
    "2H_HOLYSTAFF_UNDEAD": "Redemption Staff",
    "2H_SHAPESHIFTER_SET2": "Rootbound Staff",
    "MAIN_CURSEDSTAFF_AVALON": "Shadowcaller",
    "2H_CROSSBOWLARGE_MORGANA": "Siegebow",
    "2H_TWINSCYTHE_HELL": "Soulscythe",
    "MAIN_SPEAR": "Spear",
    "2H_KNUCKLES_SET3": "Spiked Gauntlets",
    "2H_HARPOON_HELL": "Spirithunter",
    "2H_ROCKSTAFF_KEEPER": "Staff of Balance",
    "2H_HAMMER_UNDEAD": "Tombhammer",
    "TRASH": "Trash",
    "2H_TRIDENT_UNDEAD": "Trinity Spear",
    "2H_KNUCKLES_KEEPER": "Ursine Maulers",
    "2H_BOW_HELL": "Wailing Bow",
    "2H_WARBOW": "Warbow",
    "2H_REPEATINGCROSSBOW_UNDEAD": "Weeping Repeater",
    "2H_LONGBOW_UNDEAD": "Whispering Bow",
    "2H_WILDSTAFF": "Wild Staff",
    "MAIN_FIRESTAFF_KEEPER": "Wildfire Staff",
    "MAIN_ARCANESTAFF_UNDEAD": "Witchwork Staff",
};

export type WeaponOption = { label: string; value: string };

export const weaponOptions: WeaponOption[] = Object.keys(weapons).map(k => ({
    value: k,
    label: weapons[k],
}));

export function useWeaponOptions(data: BuildStats['builds'] | WeaponMatrix | null) {
    const [options, setOptions] = useState<WeaponOption[]>([]);

    useEffect(() => {
        if (data === null) {
            return;
        }

        const usedWeapons = Array.isArray(data)
            ? new Set(data.map((b) => b.build.main_hand.type))
            : new Set(Object.keys(data));

        const filteredOptions = weaponOptions.filter(
            ({ value }) => usedWeapons.has(value) || value === ''
        );

        setOptions(filteredOptions);
    }, [data]);

    return options;
}
