import { Event } from 'src/models/event'

export interface Home {
    juicy_kills: Event[];
    high_rank_cds: Event[];
    streamed_fights: Event[];
    last_update: Date;
}

export interface StreamsResponse {
    streams: TwitchStream[];
    last_update: string;
}

export interface TwitchStream {
    albion_name: string;
    stream_url: string;
    twitch_name: string;
    title: string;
    viewer_count: number;
    started_at: Date;
    language: string;
    thumbnail_url: string;
    events: StreamEvent[];
}

interface StreamEvent {
    time: number;
    id: number;
    weapon: string;
    is_kill: boolean;
}
