export function filterDict(fn: (v: any, key: string) => boolean, dict: { [key: string]: any }) {
    return Object.keys(dict).reduce((filtered: { [key: string]: any }, key) => {
        if (fn(dict[key], key)) {
            filtered[key] = dict[key];
        }
        return filtered;
    }, {});
}
