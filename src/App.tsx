import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';

import { AppBar } from 'src/components/AppBar';
import { SideBar } from 'src/components/SideBar';
import { routes } from 'src/routes';
import { Footer } from 'src/components/Footer';

function App() {
    return (
        <Router>
            <Layout>
                <SideBar />
                <Layout>
                    <Layout.Header style={{ backgroundColor: 'rgb(83, 25, 5)', display: 'flex', alignItems: 'center' }}>
                        <Switch>
                            {routes.map((route, index) => (
                                <Route key={index} path={route.path} children={<AppBar />} />
                            ))}
                        </Switch>
                    </Layout.Header>
                    <Layout.Content className="content">
                        <Switch>
                            {routes.map((route, index) => (
                                <Route key={index} path={route.path} children={<route.main />} />
                            ))}
                        </Switch>
                    </Layout.Content>
                    <Footer />
                </Layout>
            </Layout>
        </Router>
    );
}

export default App;
