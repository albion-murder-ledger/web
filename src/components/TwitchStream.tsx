import { CSSProperties } from 'react';
import { Link } from 'react-router-dom';

import { GenericItem } from 'src/components/Item';
import { TwitchStream as StreamModel } from 'src/models/home';

type TwitchStreamProps = { stream: StreamModel };

export const TwitchStream = ({ stream }: TwitchStreamProps) => {
    const thumb = stream.thumbnail_url.replace("{width}", "800").replace("{height}", "450");

    const thumbnailStyle: CSSProperties = {
        width: "100%",
        aspectRatio: "16 / 9",
        position: "relative",
        border: "2px solid purple",
        backgroundImage: `url("${thumb}")`,
        backgroundColor: "purple",
        backgroundSize: "cover",
    };

    const thumbnailTagStyle: CSSProperties = {
        position: "absolute",
        backgroundColor: "rgba(0, 0, 0, 0.8)",
        padding: "1px 3px",
        borderRadius: "2px",
    }

    let weaponIcon = null;
    let kdTag = null;

    if (stream.events.length > 0) {
        const weapon = stream.events[0].weapon;
        weaponIcon = (
            <div style={{ ...thumbnailTagStyle, top: "5px", left: "5px" }}>
                <GenericItem item={weapon} size={40} />
            </div>
        );

        const numKills = stream.events.reduce((total, event) => event.is_kill ? total + 1 : total, 0);
        const numDeaths = stream.events.length - numKills;
        kdTag = (
            <div style={{ ...thumbnailTagStyle, top: "5px", right: "5px" }}>
                K: {numKills}, D: {numDeaths}
            </div>
        );
    }

    return (
        <div>
            <a href={stream.stream_url} title={stream.title}>
                <div style={thumbnailStyle}>
                    <div style={{ ...thumbnailTagStyle, bottom: "5px", left: "5px" }}>{stream.viewer_count} viewers</div>
                    <div style={{ ...thumbnailTagStyle, bottom: "5px", right: "5px" }}>{stream.language}</div>
                    {kdTag}
                    {weaponIcon}
                </div>
            </a>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <span>
                    <a href={stream.stream_url}>
                        twitch.tv/{stream.twitch_name}
                    </a>
                </span>
                <span>
                    <Link to={`/players/${stream.albion_name}/ledger`}>
                        {stream.albion_name}
                    </Link>
                </span>
            </div>
        </div>
    );
};
