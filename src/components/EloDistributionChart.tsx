import { EloDistribution } from 'src/models/leaderboard';
import React from 'react';
import { Area, AreaConfig } from '@ant-design/charts';

type Props = { data: EloDistribution['data'] };
export const EloDistributionChart: React.FC<Props> = ({ data }) => {
    const config: AreaConfig = {
        data: Object.entries(data).map(([elo, Players]) => ({ elo, Players })),
        xField: 'elo',
        yField: 'Players',
        appendPadding: [0, 2, 0, 0], // Last tick text is cutting off without that
        color: 'rgb(153, 90, 69)',
        xAxis: {
            line: { style: { opacity: 0.3 } },
        },
        yAxis: {
            grid: { line: { style: { opacity: 0.1 } } },
        },
        height: 128,
    };
    return <Area {...config} />;
};
