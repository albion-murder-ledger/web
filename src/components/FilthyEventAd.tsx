import {useEffect} from 'react';

export const FilthyEventAd = () => {
    useEffect(() => {
        // @ts-ignore
        (window.adsbygoogle = window.adsbygoogle || []).push({});
    }, []);

    return (
        <ins
            className="adsbygoogle"
            style={{ display: 'block', height: '130px', marginBottom: '10px' }}
            data-ad-client="ca-pub-7353288407516672"
            data-full-width-responsive="true"
            data-ad-format="fluid"
            data-ad-layout-key="-f9+5v+4m-d8+7b"
            data-ad-slot="6425986907">
        </ins>
    );
}
