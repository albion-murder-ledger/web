import { useEffect, useState, useMemo } from 'react';
import { DateTime } from "luxon";
import { Space, Alert, Tabs } from 'antd';
import { useParams, useHistory, Link } from 'react-router-dom';
import humanizeDuration from 'humanize-duration';

import { getRelativeTime } from 'src/util/relativeTime';
import { Season } from 'src/models/leaderboard';
import { api } from 'src/api';
import { FilthyAd } from 'src/components/FilthyAd';
import { Typography } from 'antd';
import { Leaderboard1v1Page } from 'src/pages/leaderboards/Leaderboard1v1Page';
import { Leaderboard2v2Page } from 'src/pages/leaderboards/Leaderboard2v2Page';

const { Title } = Typography;

export const LeaderboardPage = () => {
    const history = useHistory();
    const { tab } = useParams<{ tab: string }>();

    const [seasons, setSeasons] = useState<Array<Season>>([]);
    const [currentSeason, setCurrentSeason] = useState<Season | null>(null);
    const [syncDelay, setSyncDelay] = useState<number>(0);

    const [error, setError] = useState<string | null>(null);

    useEffect(() => {
        api<Array<Season>>(`/seasons`)
            .then((data) => {
                setSeasons(data);
                for (const season of data) {
                    if (season.is_current) {
                        setCurrentSeason(season);
                        break;
                    }
                }
            })
            .catch((error) => setError(String(error)));
    }, []);

    const seasonAlert = useMemo(() => {
        if (currentSeason === null) {
            return null;
        }

        const seasonEnd = DateTime.fromSeconds(currentSeason.end_date);
        if (seasonEnd.diffNow().as('days') > 8) {
            return null;
        }

        const relDisplay = getRelativeTime(new Date(currentSeason.end_date * 1000));

        return (
            <Alert
                message={`The current season ends ${relDisplay}. The current leaderboard will be archived at that time and player ratings will be reset.`}
                type="warning"
                showIcon
                closable
            />
        );
    }, [currentSeason]);

    const syncDelayEl = useMemo(() => {
        const delay = humanizeDuration(syncDelay * 1000, { units: ["d", "h", "m"], round: true });

        if (syncDelay > 1800) {
            return (
                <div title="Albion API is running behind" style={{ color: 'red' }}>Sync Delay: {delay}</div>
            );
        }

        return (
            <div>Sync Delay: {delay}</div>
        );
    }, [syncDelay]);

    if (error) {
        return (
            <div style={{ marginTop: '80px' }}>
                <Alert
                    type="error"
                    message={
                        'There was an error loading data'
                    }
                    description={error}
                    banner
                />
            </div>
        );
    }

    return (
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
            <Space direction="vertical" style={{ maxWidth: '1100px', width: '100%' }}>
                <FilthyAd slot="3788215273" />
                <Title>Leaderboards</Title>
                <Title level={4}>
                    <strong>{currentSeason?.name ?? 'Current Season'}</strong> ends on {DateTime.fromSeconds(currentSeason?.end_date ?? 0).toFormat('LLL d')}  <Link to="/seasons">Information ℹ️ and Rewards 🏆</Link>
                </Title>
                {seasonAlert}
                <Tabs
                    activeKey={tab}
                    defaultActiveKey={tab}
                    onChange={(t: string) => history.replace(`/leaderboards/${t}`)}
                    tabBarExtraContent={syncDelayEl}
                >
                    <Tabs.TabPane tab="1v1" key="1v1">
                        {tab === '1v1' ? <Leaderboard1v1Page seasons={seasons} onSyncDelayChange={setSyncDelay} /> : null}
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="2v2" key="2v2">
                        {tab === '2v2' ? <Leaderboard2v2Page seasons={seasons} onSyncDelayChange={setSyncDelay} /> : null}
                    </Tabs.TabPane>
                </Tabs>
            </Space>
        </div>
    );
};
