import { Typography, Table, Space } from 'antd';
import { Link } from 'react-router-dom';

import { FilthyAd } from 'src/components/FilthyAd';

const { Title, Paragraph, Text } = Typography;

const seasons = [
    {
        season: '2021S1',
        key: '2021S1',
        name: '2021 Season 1',
        start_date: '2021-01-09',
        end_date: '2021-09-30',
        prizes: 'none',
        sponsors: 'none',
    },
    {
        season: '2021S2',
        key: '2021S2',
        name: '2021 Season 2',
        start_date: '2021-10-01',
        end_date: '2021-12-31',
        prizes: '50M',
        sponsors: 'imbafromNorth',
    },
    {
        season: '2022S1',
        key: '2022S1',
        name: '2022 Season 1',
        start_date: '2022-01-01',
        end_date: '2022-03-31',
        prizes: 'none',
        sponsors: 'none',
    },
];

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Start',
        dataIndex: 'start_date',
        key: 'start',
    },
    {
        title: 'End',
        dataIndex: 'end_date',
        key: 'end',
    },
    {
        title: 'Prizes',
        dataIndex: 'prizes',
        key: 'prizes',
    },
    {
        title: 'Sponsors',
        dataIndex: 'sponsors',
        key: 'sponsors',
        render: (text: string) => {
            if (text === "none") {
                return text;
            }

            return (
                <Link to={`/players/${text}/ledger`}>{text}</Link>
            );
        },
    },
];

export const SeasonsPage = () => (
    <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '30px' }}>
        <Typography style={{ maxWidth: '1000px' }}>
            <Title>Leaderboard Seasons</Title>
            <Paragraph>
                Leaderboard seasons are 3 months long, making for 4 seasons in a year. The first, unofficial, season was from Jan 9th 2021 to Sept 30th 2021. All subsequent seasons are 3 months.
            </Paragraph>
            <Space direction="vertical" style={{ width: '100%' }} >
                <Table dataSource={seasons} columns={columns} pagination={false} />
                <FilthyAd slot="3788215273" />
            </Space>
            <Title level={3}>2021 Season 2</Title>
            <Title level={5}>imbafromNorth</Title>
            <Paragraph>
                Stalker:
                <ul>
                    <li>1st: <Text strong>25 Million Silver</Text></li>
                    <li>2nd: <Text strong>15 Million Silver</Text></li>
                    <li>3rd: <Text strong>10 Million Silver</Text></li>
                </ul>
            </Paragraph>
        </Typography>
    </div>
);
