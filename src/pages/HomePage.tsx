import { useState, useEffect, KeyboardEvent } from 'react';
import { Col, Row, Select, Space, List, notification, Button, Typography } from 'antd';
import { TrophyOutlined } from '@ant-design/icons';
import { History } from 'history'
import { Link, useHistory } from 'react-router-dom';

import { api } from 'src/api';
import { useDebounceEffect } from 'src/util/useDebounceEffect';
import { Home as HomeModel, StreamsResponse, TwitchStream as StreamModel } from 'src/models/home';
import { Event } from 'src/models/event';
import { Item } from 'src/components/Item';
import { TwitchStream } from 'src/components/TwitchStream';
import { TwitchLink } from 'src/components/TwitchLink';
import { FilthyAd } from 'src/components/FilthyAd';

function openNotification(history: History) {
    const lastNoti = localStorage.getItem('homepage-1v1-season-last-noti');
    if (!lastNoti) {
        localStorage.setItem('homepage-1v1-season-last-noti', Date.now().toString());
        const key = `open${Date.now()}`;
        const btn = (
            <Button
                type="primary"
                onClick={() => {
                    notification.close(key);
                    history.push('/seasons');
                }}
            >
                Details
            </Button>
        );
        notification.open({
            message: 'New 1v1 Season',
            description:
                'A new 1v1 corrupted dungeon season has begun! With prizes by imbafromNorth! ',
            btn,
            key,
            top: 80,
            duration: 10,
            icon: <TrophyOutlined />
        });
    }
};

export const HomePage = () => {
    const history = useHistory();
    const [home, setHome] = useState<HomeModel | null>(null);
    const [streams, setStreams] = useState<StreamModel[]>([]);
    const [suggestions, setSuggestions] = useState<string[]>([])
    const [q, setQ] = useState<string>('')

    const fetchSuggestions = async () => {
        if (q.length < 3) {
            setSuggestions([]);
            return;
        }
        const result = await api<{ results: string[] }>(`/player-search/${q}`);
        setSuggestions(result.results)
    };

    const handleSelect = (name: string) => {
        setQ('');
        history.push(`/players/${name}/ledger`);
    };

    const handleKey = ({ key }: KeyboardEvent) => {
        if (key === 'Enter' && suggestions.length === 0) {
            setQ('');
            history.push(`/players/${q}/ledger`);
        }
    };

    useDebounceEffect(
        () => fetchSuggestions(),
        150,
        [q],
    );


    useEffect(() => {
        api<HomeModel>('/home')
            .then(home => {
                setHome(home);
            });
        api<StreamsResponse>('/streams')
            .then(streams => {
                setStreams(streams.streams);
            });
    }, [history]);

    return (
        <>
            <Row justify="center" style={{ marginTop: '35px', marginBottom: '25px' }} >
                <Col xs={24} sm={20} md={12} lg={8}>
                    <Space direction="vertical" style={{ width: '100%' }}>
                        <span>Player Search</span>
                        <Select
                            size="large"
                            placeholder="Player Search"
                            showSearch
                            autoFocus
                            onSearch={s => setQ(s)}
                            onInputKeyDown={handleKey}
                            onSelect={handleSelect}
                            showArrow={false}
                            style={{ width: '100%', backgroundColor: 'rgba(0,0,0,0.5)' }}
                        >
                            {suggestions.map(name => (
                                <Select.Option key={name} value={name}>{name}</Select.Option>
                            ))}
                        </Select>
                    </Space>
                </Col>
            </Row>
            <FilthyAd slot="3788215273" />
            <Row gutter={[{ sm: 7, md: 15, lg: 25 }, 15]} style={{ marginTop: '25px', marginBottom: '25px' }}>
                <Col xs={24} sm={24} md={24} lg={12} xl={8} >
                    <List
                        size="large"
                        dataSource={home?.juicy_kills ?? []}
                        header={(<strong>Recent Juicy Kills</strong>)}
                        bordered
                        renderItem={(event: Event) => (
                            <List.Item key={event.id}>
                                <Link to={`/players/${event.killer.name}/ledger?q=${event.victim.name}`}>
                                    <Space style={{ width: '150px', overflow: 'hidden' }}>
                                        <Item item={event.killer.loadout.main_hand} size={40} />
                                        <strong title={event.killer.name}>{event.killer.name}</strong>
                                    </Space>
                                </Link>
                                <span>killed</span>
                                <Link to={`/players/${event.victim.name}/ledger?q=${event.killer.name}`}>
                                    <Space style={{ width: '150px', overflow: 'hidden' }}>
                                        <Item item={event.victim.loadout.main_hand} size={40} />
                                        <strong title={event.victim.name}>{event.victim.name}</strong>
                                    </Space>
                                </Link>
                                <a href={`https://albiononline.com/en/killboard/kill/${event.id}`} target="_blank" rel="noreferrer">
                                    <span><strong>{event.total_kill_fame.toLocaleString()}</strong> fame</span>
                                </a>
                            </List.Item>
                        )}
                    >
                    </List>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8} >
                    <List
                        size="large"
                        dataSource={home?.high_rank_cds ?? []}
                        header={(<strong>Recent High Rank CD Fights</strong>)}
                        bordered
                        renderItem={(event: Event) => (
                            <List.Item key={event.id}>
                                <Link to={`/players/${event.killer.name}/ledger?q=${event.victim.name}`}>
                                    <Space style={{ width: '160px', overflow: 'hidden' }}>
                                        <Item item={event.killer.loadout.main_hand} size={40} />
                                        <span>#{event.killer.rank_1v1}</span>
                                        <strong title={event.killer.name}>{event.killer.name}</strong>
                                    </Space>
                                </Link>
                                <span>killed</span>
                                <Link to={`/players/${event.victim.name}/ledger?q=${event.killer.name}`}>
                                    <Space style={{ width: '160px', overflow: 'hidden', justifyContent: 'end' }}>
                                        <span>#{event.victim.rank_1v1}</span>
                                        <strong title={event.victim.name}>{event.victim.name}</strong>
                                        <Item item={event.victim.loadout.main_hand} size={40} />
                                    </Space>
                                </Link>
                            </List.Item>
                        )}
                    >
                    </List>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8} >
                    <List
                        size="large"
                        dataSource={home?.streamed_fights ?? []}
                        header={(<strong>Recent Streamed Kills</strong>)}
                        bordered
                        renderItem={(event: Event) => (
                            <List.Item key={event.id}>
                                <Link to={`/players/${event.killer.name}/ledger?q=${event.victim.name}`}>
                                    <Space style={{ width: '150px' }}>
                                        <Item item={event.killer.loadout.main_hand} size={40} />
                                        <strong>{event.killer.name}</strong>
                                    </Space>
                                </Link>
                                <span>killed <TwitchLink link={event.killer.vod ? event.killer.vod : event.victim.vod} /></span>
                                <Link to={`/players/${event.victim.name}/ledger?q=${event.killer.name}`}>
                                    <Space style={{ width: '150px', justifyContent: "end" }}>
                                        <strong>{event.victim.name}</strong>
                                        <Item item={event.victim.loadout.main_hand} size={40} />
                                    </Space>
                                </Link>
                            </List.Item>
                        )}
                    >
                    </List>
                </Col>
            </Row>
            <Row>
                <Typography.Title level={4}>Live Streams</Typography.Title>
            </Row>
            <Row gutter={[16, 16]}>
                {streams.map(stream => (
                    <Col key={stream.twitch_name} xs={24} sm={12} md={12} lg={8} xl={6}><TwitchStream stream={stream} /></Col>
                ))}
            </Row>
        </>
    );
};
