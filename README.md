# Albion Murder Ledger

This web app shows player kill history backed by [Albion Kills API](https://gitlab.com/findley/albion-kills-api)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
